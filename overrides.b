FINAL_OUTPUT_FILENAME=hwreg

benvFirstObjects = \


benvExtraObjects = \


LIB_INC_ROOTS=  -I$(srcroot)/src \
				-I$(srcroot)/tools/benvc/$(HOST)/$(HOSTMACH) \
				-I$(srcroot)/src/oscl/platform/$(HOSTMACH) \
				-I$(srcroot)/src/oscl/platform/compiler/GCC/$(shell uname -m) \
				-I$(srcroot)/src/oscl/platform/posix \
				-I$(srcroot)/ \
				-I$(srcroot)/src/mnm/hwreg

$(info srcroot:"$(srcroot)")

EXTRA_CLEANABLE_OBJECTS_LIST=first lexer.cpp parser.cpp parser.hpp

DEBUG_CFLAG=-g
